### mkusers
Ansible playbook which allows you to create users, add them to proper groups and deploy their ssh pubkeys automatically.

#### how to
* Edit `user_list.yml` and adjust it to your needs. Every variable is mandatory except `groups`, it can be skipped, example:
```
user_list:
  - { username: "test", shell: "/bin/bash", groups: "sudo" }
  - { username: "anon", shell: "/bin/bash" }
```
* Put porper keys in `pub_keys` directory for previously defined users. Naming convention: `<username>.pub`, example: `anon.pub`
* Run this playbook (mkusers.yml), example: `ansible-playbook mkusers.yml -u <remoteuser>`
